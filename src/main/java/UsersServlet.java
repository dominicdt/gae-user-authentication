import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@WebServlet(
    name = "UsersServlet",
    urlPatterns = {"/users"}
)
public class UsersServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
      
	  UserService userService = UserServiceFactory.getUserService();

	    String thisUrl = request.getRequestURI();

	    response.setContentType("text/html");
	    if (request.getUserPrincipal() != null) {
	    	response.getWriter()
	          .println(
	              "<p>Hello, "
	                  + request.getUserPrincipal().getName()
	                  + ".  <a href=\""
	                  + userService.createLogoutURL(thisUrl)
	                  + "\">sign out</a>.</p>");
	    } else {
	    	response.getWriter()
	          .println(
	              "<p>Hello, guest. Please <a href=\"" + userService.createLoginURL(thisUrl) + "\">sign in</a>.</p>");
	    }
	}
}